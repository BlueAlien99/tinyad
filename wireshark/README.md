## TinyAd - Wireshark Dissector Plugin

### Installation

Place _tinyad-protocol.lua_ script to `/root/.local/lib/wireshark/plugins`.

If some reason this won't work, open Wireshark and go to: 
`Help -> About Wireshark -> Folders` and search for _Personal Lua Plugins_ location. Place the script there.